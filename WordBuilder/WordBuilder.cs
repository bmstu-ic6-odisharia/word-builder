﻿using System.Linq;

namespace WordBuilder
{
    public class WordBuilder {
        /// <summary>
        /// Можно ли построить слово smallWord из букв, входящих в слово longWord 
        /// (каждую букву слова longWord можно использовать только один раз).
        /// Регистр слов не учитывается: слово "Река" можно построить из слова "Америка".
        /// <param name="smallWord">Слово, которое требуется построить.</param>
        /// <param name="longWord">Слово, из букв которого необходимо построить smallWord.</param>
        /// </summary>
        public static bool CouldBuild(string longWord, string smallWord) {
            var longWordDict = longWord.ToLower().ToCharArray(0, longWord.Length).GroupBy(s => s).Select( s => new { 
                    Stuff = s.Key, Count = s.Count()
                }).ToDictionary(key => key.Stuff, key => key.Count);
            var smallWordDict = smallWord.ToLower().ToCharArray(0, smallWord.Length).GroupBy(s => s).Select( s => new { 
                    Stuff = s.Key, Count = s.Count()
                }).ToDictionary(key => key.Stuff, key => key.Count);
                
            int count;
            var dict3 = smallWordDict.Where(entry => longWordDict.TryGetValue(entry.Key, out count))
                .Where(entry => longWordDict[entry.Key] >= entry.Value)
                .ToDictionary(entry => entry.Key, entry => entry.Value);

            if (dict3.Count() == smallWordDict.Count()) {
                return true;
            } else {
                return false;
            }
        }
    }
}
