# Написать статический метод класса

```csharp
public static bool CouldBuild(string longWord, string smallWord) {};
```

метод должен проверять, можно ли построить слово `smallWord` из букв, входящих в слово `longWord` (каждую букву слова `longWord` можно использовать только один раз).

## Восстановление проекта

```
dotnet restore WordBuilder
dotnet restore WordBuilder.Tests
```

## Запуск тестов

```
dotnet test WordBuilder.Tests/WordBuilder.Tests.csproj 
```
