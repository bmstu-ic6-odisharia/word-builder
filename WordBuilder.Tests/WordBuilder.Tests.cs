using System;
using Xunit;

namespace WordBuilder.Tests
{
    public class WordBuilder_CouldBuild
    {   
        /// <summary>
        /// Проверяем корректность работы функции при длине маленького слова большей, чем у большого слова.
        /// </summary>
        [Theory]
        [InlineData("Int", "ShortInt")]
        [InlineData("Lap", "Alps")]
        [InlineData("God", "Goodness")]
        public void ReturnFalseIfGivenSmallWordLongerThenLongWord(string longWord, string smallWord)
        {
            bool result = WordBuilder.CouldBuild(longWord, smallWord);
            Assert.False(result);
        }
        
        /// <summary>
        /// Проверяем корректность работы функции при "нормальных" условиях.
        /// </summary>
        [Theory]
        [InlineData("Knife", "Fine")]
        [InlineData("Snapdragon", "Pardon")]
        [InlineData("Failure", "Failure")]
        [InlineData("Foo", "Foo")]
        [InlineData("Inline", "Niline")]
        [InlineData("Srut", "Rust")]
        public void ReturnTrueIfSmallWordCouldBuildFromLongWord(string longWord, string smallWord)
        {
            bool result = WordBuilder.CouldBuild(longWord, smallWord);
            Assert.True(result, $"{smallWord} should be possible to build from {longWord})");
        }
        
        /// <summary>
        /// Проверяем, что при составлении маленького слова из букв большого эти буквы используются единожды.
        /// </summary>
        [Theory]
        [InlineData("Duster", "Studu")]
        [InlineData("Preoccupations", "Partition")]
        [InlineData("Unwound", "Wowod")]
        public void VerifyThatTheLetterOfTheLongWordIsUsedOnce(string longWord, string smallWord)
        {
            bool result = WordBuilder.CouldBuild(longWord, smallWord);
            Assert.False(result, $"{smallWord} should use one letter from another word {longWord})");
        }
    }
}
